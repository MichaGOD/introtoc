#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

int numberSqrt(int number)
{
	return floor(sqrt(number));
}

typedef struct Rectangle {
	int length;
	int width;
} Rectangle;

bool canFitInEachother(Rectangle rec1, Rectangle rec2)
{
	return (rec1.length <= rec2.length && rec1.width <= rec2.width) || (rec1.length >= rec2.length && rec1.width >= rec2.width);
}

int getRectangleArea(Rectangle rec)
{
	return rec.length * rec.width;
}

Rectangle getBiggerRectangle(Rectangle rec1, Rectangle rec2)
{
	if (getRectangleArea(rec1) > getRectangleArea(rec2))
		return rec1;
	return rec2;
}

typedef struct Point {
	int x;
	int y;
} Point;

Point furtherPoint(Point p1, Point p2)
{
	int p1dist = (p1.x * p1.x) + (p1.y * p1.y);
	int p2dist = (p2.x * p2.x) + (p2.y * p2.y);
	if (p1dist > p2dist)
		return p1;
	return p2;
}

int fibValue(int index)
{
	if (index == 2 || index == 1) {
		return 1;
	}
	return fibValue(index - 1) + fibValue(index - 2);
}

int get_fib_index(int n)
{
	int i = 1;
	int fibNum = fibValue(i);
	for (i = 2; fibNum < n; i++)
	{
		fibNum = fibValue(i);
	}
	if (fibNum == n) {
		return --i;
	}
	return -1;
}

void printMamasEmpire()
{
	int numberOfCycles = 53;
	for (int i = 1; i <= numberOfCycles; i++)
	{
		printf("Mamas %d Empire!\n", i);
	}
}

int main() {
	
	printf("%d", numberSqrt(17));
	return 0;
}
